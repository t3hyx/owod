function formatDate_(date) {
  let d = Utilities.formatDate(new Date(date), "Europe/Paris", "yyyy-MM-dd");
  return d;
}
