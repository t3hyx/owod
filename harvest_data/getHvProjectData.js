function getHvProjectData() {
  /***************************************
   *        VARIABLES STATEMENTS         *
   **************************************/

  const HV_API_DOM = "https://api.harvestapp.com/v2";
  const HV_ACC_ID = "1185444";
  const HV_API_TOKEN =
    "2398286.pt.fSsXN4Rn6lTD2B6G3oNHlDth3jEdsppZBFpAe8LJWQvel4JXAsU4M-pNQawZsakqDnDN5JcjZycmiWmsAxHHRA";
  const HV_REQ_HEADERS = {
    "Harvest-Account-ID": HV_ACC_ID,
    Authorization: `Bearer ${HV_API_TOKEN}`,
    "User-Agent": "OWOD-v3 (tdubois@optimalways.com)",
  };
  const HV_REQ_PAGE_CAP = 2000;
  const HV_REQ_URIS = {
    projects: ["/projects", HV_REQ_PAGE_CAP],
    users: ["/users", HV_REQ_PAGE_CAP],
    task_ass: ["/task_assignments", HV_REQ_PAGE_CAP],
    users_ass: ["/user_assignments", HV_REQ_PAGE_CAP],
    time_report: ["/time_entries", HV_REQ_PAGE_CAP],
    _misc: { pagination: { page: "?page=", per_page: "&per_page=" } },
  };
  const SS = SpreadsheetApp.getActive();

  let output_proj = [];

  /***************************************
   *        FUNCTIONS DECLARATION        *
   **************************************/

  /**
   * ToDo.
   * @param {xxxxx} xxxxxx - xxxxxxxxxxxxxxx.
   */
  const getProjData_ = () => {
    let j = 1;
    do {
      let url = apiDomain + domainUri[0] + j;
      res = JSON.parse(UrlFetchApp.fetch(url, hvOptions).getContentText());
      res["projects"].forEach((el) => {
        projOutput.push([
          el.id,
          el.client["name"],
          el.code,
          el.name,
          el.is_active,
          el.is_billable,
          el.budget,
          formatDate_(el.starts_on),
          formatDate_(el.ends_on),
          formatDate_(el.created_at),
          formatDate_(el.updated_at),
        ]);
      });
      j += 1;
    } while (res.next_page !== null);
  };

  /**************************************
   *             SCRIPT START            *
   **************************************/

  getProjData_();
  Logger.log("end");
}
